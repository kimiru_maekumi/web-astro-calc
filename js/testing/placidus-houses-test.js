
const CUSP_NAMES = ["AC",2,3,"IC",5,6,"DC",8,9,"MC",11,12];

const CUSP_OFFSETS = {
    'a': 0,
    'b': 30,
    'c': 60,
    'd': 90,
    'e': 120,
    'f': 150,
    'g': 180,
    'h': 210,
    'i': 240,
    'j': 270,
    'k': 300,
    'l': 330
}

function runPlacidusHouseTests(){

    let natal_chart = new NatalChart("Woody Allen",-1075579500,"-73.9, 40.85");
    console.log(natal_chart.toArray());

    return;

    let actual_cusps;

    let placidus_houses;
    let string = "";
    let difference = 0;

    let max_difference = 0;

    for (let i = 0; i < test_samples.length; i++) {

        placidus_houses = new PlacidusHouses(test_samples[i].name, utcToSiderealDegrees(test_samples[i].utc, test_samples[i].longitude), test_samples[i].latitude, test_samples[i].ecliptic_obliquity);
        // placidus_houses = new PlacidusHouses(utcToSiderealDegrees(test_samples[i].utc, test_samples[i].longitude,true), test_samples[i].latitude, test_samples[i].ecliptic_obliquity);
        actual_cusps = placidus_houses.cusps;

        string +="Test results ("+test_samples[i].name+"):\n";
        difference = test_samples[i].ramc_degrees - placidus_houses.ramc_degrees;
        // string +="Actual RAMC\tExpected RAMC\tDifference\n";
        // string += placidus_houses.ramc_degrees +"\t" + test_samples[i].ramc_degrees +"\t" + difference.toFixed(5) +"\n";
        // string += "sidereal degrees " + utcToSiderealDegrees(test_samples[i].utc, test_samples[i].longitude) +"\n";
        // string += "sidereal degrees " + utcToSiderealDegrees(test_samples[i].utc, test_samples[i].longitude, true) +"\n";
        string +="Cusp\tActual\tExpected\tDifference\n";
        for (let j = 0; j < 12; j++) {
            difference = Math.abs(test_samples[i].house_divisions[j] - actual_cusps[j]);
            string += CUSP_NAMES[j] +"\t" + actual_cusps[j] +"\t" + test_samples[i].house_divisions[j] +"\t" + difference.toFixed(5) +"\n";
            if(difference > max_difference) max_difference = difference;
        }
        string += "\n";
    }
    console.log(string+"\n");
    console.log("MAX DIFFERENCE = "+max_difference);
}

//Call function when loading document
$( window ).on( "load", function() {
    runPlacidusHouseTests();
});