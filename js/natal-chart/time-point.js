/* Reflects a moment in time with different ways to track time */

class TimePoint {

    constructor(unix_timestamp){
        this.unix_timestamp = unix_timestamp;

        //Calculate Julian date
        //http://www.jgiesen.de/astro/astroJS/siderealClock/
        let julian_base_day = 2451545;
        this.julian_date = unix_timestamp / 86400 + 2440587.5;    //Timestamp in seconds is converted to days and base Julian Day added
        this.julian_century = (this.julian_date / 365.25) * 0.010000205343026;  //http://conversion.org/time/year-julian/century    Julian year divided by ~100

        //Calculate sidereal time
        //http://www.agopax.it/Libri_astronomia/pdf/Astronomical%20Algorithms.pdf
        //Jean Meeus "Astronomical Algorithms" formula 12.3
        let T = (this.julian_date - julian_base_day) / 36525;
        this.theta0 = 280.46061837 + 360.98564736629 * (this.julian_date - julian_base_day) + 0.000387933 * (T*T) - (T*T*T) / 38710000;
        this.greenwich_sidereal_time = new Angle(this.theta0);

    }

    setSiderealTime(longitude){
        //Test at http://www.jgiesen.de/astro/astroJS/siderealClock/
        this.sidereal_time = new Angle(this.theta0 + longitude);
    }

    toArray(){
        if(this.sidereal_time === undefined) {
            return {
                'time-point': {
                    'unix-timestamp': this.unix_timestamp,
                    'julian-date': this.julian_date,
                    'greenwich-mean-sidereal-time': this.greenwich_sidereal_time.toArray(),
                    'local-sidereal-time': this.sidereal_time,
                }
            }
        }
        return {
            'time-point': {
                'unix-timestamp': this.unix_timestamp,
                'julian-date': this.julian_date,
                'greenwich-mean-sidereal-time': this.greenwich_sidereal_time.toArray(),
                'local-sidereal-time': this.sidereal_time.toArray(),
            }
        }
    }
}