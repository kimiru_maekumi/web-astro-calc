/* Holds an angle in decimal degrees and converts it to degrees, minutes and seconds */

class Angle {

    constructor(angle){
        this.decimal_degrees = this.normalizeAngle(angle);  //Normalize decimal to be within 0 and 360

        this.degrees = Math.floor(this.decimal_degrees);
        this.minutes = Math.floor((this.decimal_degrees - this.degrees) * 60);
        this.seconds = Math.floor((this.decimal_degrees - this.degrees - this.minutes / 60) * 3600);
    }

    normalizeAngle(degrees){
        degrees %= 360;
        if(degrees < 0) degrees += 360;
        return degrees;
    }

    minutesToString(){
        if(this.minutes.toString().length === 1) return '0'+this.minutes;
        return this.minutes;
    }
    secondsToString(){
        if(this.seconds.toString().length === 1) return '0'+this.seconds;
        return this.seconds;
    }
    angleToDecimal(){
        return this.degrees + (this.minutes / 60) + (this.seconds / 3600);
    }

    toString(){
        return this.degrees+'°'+this.minutesToString()+"'"+this.secondsToString()+'"';
    }
    toArray(){
        return {
            'angle': {
                'decimal': this.decimal_degrees,
                'degrees': this.degrees,
                'minutes': this.minutes,
                'seconds': this.seconds,
            }
        }
    }
}