/*
* Gets house cusps degrees using Placidus House system
*/

/*
House Cusps enumerator
*/
const EclipticPoints = {
    AC: 0,
    IC: 3,
    DC: 6,
    MC: 9,
};

function getHouseCusps(sidereal_time, latitude, ecliptic_obliquity){
    let cusps = [];

    //QUADRANT DEFINING POINTS

    let dividend;
    let divisor;

    //AC    https://en.wikipedia.org/wiki/Ascendant#Calculation
    dividend = -cos(sidereal_time);
    divisor = sin(sidereal_time) * cos(ecliptic_obliquity) + tan(latitude) * sin(ecliptic_obliquity);
    let ascendant = arctan(dividend/divisor) + 180;
    (divisor > 0) && (ascendant += 180); //First rule
    (ascendant < 180) ? ascendant += 180 : ascendant -= 180;    //Second rule
    cusps[EclipticPoints.AC] = ascendant;

    //MC
    dividend = tan(sidereal_time);
    divisor = cos(ecliptic_obliquity);
    let medium_coeli = arctan(dividend/divisor);
    if(medium_coeli < 0) medium_coeli += 180;   //First rule
    if(sidereal_time > 180) medium_coeli += 180;    //Second rule
    cusps[EclipticPoints.MC] = medium_coeli;

    //AC DC opposites
    cusps[EclipticPoints.IC] = oppositeCusp(cusps[EclipticPoints.MC]); //Imum Coeli
    cusps[EclipticPoints.DC] = oppositeCusp(cusps[EclipticPoints.AC]); //Descending

    //CUSPS BETWEEN QUADRANT DEFINING POINTS

    //Fit 2nd and 3rd House inside II. quadrant
    let cusp_pair = [];
    cusp_pair[0] = calculateHouseCusp(120, sidereal_time, latitude, ecliptic_obliquity);
    cusp_pair[1] = calculateHouseCusp(150, sidereal_time, latitude, ecliptic_obliquity);
    cusp_pair = fitCuspPairInSegment(cusp_pair, cusps[EclipticPoints.AC], cusps[EclipticPoints.IC]);
    cusps[1] = cusp_pair[0];    //2nd House
    cusps[2] = cusp_pair[1];    //3rd House

    //Fit 5th and 6th House inside III. quadrant
    cusp_pair[0] = calculateHouseCusp(30, sidereal_time, latitude, ecliptic_obliquity);
    cusp_pair[1] = calculateHouseCusp(60, sidereal_time, latitude, ecliptic_obliquity);
    cusp_pair = fitCuspPairInSegment(cusp_pair, cusps[EclipticPoints.IC], cusps[EclipticPoints.DC]);
    cusps[4] = cusp_pair[0];    //5th House
    cusps[5] = cusp_pair[1];    //6th House

    //Opposites of cusps between quadrant defining points

    cusps[7] = oppositeCusp(cusps[1]); //8th House
    cusps[8] = oppositeCusp(cusps[2]); //9th House
    cusps[10] = oppositeCusp(cusps[4]); //11th House
    cusps[11] = oppositeCusp(cusps[5]); //12th House

    return cusps;
}

function absoluteRotation(degrees){
    if(degrees < 0) return degrees + 180;
    return degrees;
}

function isInside(cusp, segment_start, segment_end){
    if(segment_start < segment_end){    //Segment doesn't cross radix and cusp can be evaluated normally
        return (cusp > segment_start && cusp <= segment_end);
    }
    else{   //Segment crosses radix and needs to be split at the point where 360 turns to 0
        return (cusp > segment_start && cusp < 360) || (cusp >= 0 && cusp <= segment_end);
    }
}
function fitCuspPairInSegment(cusps, segment_start, segment_end){
    let offset;
    do {
        cusps[0] = (cusps[0] + 90) % 360;
    } while(!isInside(cusps[0], segment_start, segment_end))
    do {
        cusps[1] = (cusps[1] + 90) % 360;
    } while(!isInside(cusps[1], cusps[0], segment_end))
    return cusps;
}

//Get the cusp on the other end of the circle
function oppositeCusp(degrees){
    if(degrees > 180) return degrees - 180;
    return degrees + 180;
}

function calculateHouseCusp(offset, sidereal_time, latitude, obliquity){
    let divisor = Math.floor(-(offset - 90) / 30) * 1.5;

    let right_ascension = sidereal_time + offset;  //First iteration

    //Next iterations until floating point value doesn't change
    for (let i = 0; i < 18; i++) {
        if(offset > 90){
            right_ascension = sidereal_time + 180 + (arccos(sin(right_ascension) * tan(obliquity) * tan(latitude)) / divisor);
        }
        else{
            right_ascension = sidereal_time + (arccos(-sin(right_ascension) * tan(obliquity) * tan(latitude)) / divisor);
        }
    }
    let cusp_degrees = arctan(tan(right_ascension) / cos(obliquity));  //Get degrees from right ascension

    if (cusp_degrees < 0) cusp_degrees += 180;   //Normalize

    return cusp_degrees;
}

function arcsin(value) { return radToDeg(Math.asin(value)); }
function arccos(value) { return radToDeg(Math.acos(value)); }
function arctan(value) { return radToDeg(Math.atan(value)); }
function arcctg(value) { return radToDeg(Math.atan(1 / value)); }
function cos(degrees) { return Math.cos(degToRad(degrees)); }
function sin(degrees) { return Math.sin(degToRad(degrees)); }
function tan(degrees) { return Math.tan(degToRad(degrees)); }
function ctg(degrees) { return 1 / Math.tan(degToRad(degrees)); }

function radToDeg(radians)
{
    return radians * (180/PI);
}
function degToRad(degrees)
{
    return degrees * (PI/180);
}