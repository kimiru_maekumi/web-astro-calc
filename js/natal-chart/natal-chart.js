const PI = Math.PI;

class NatalChart{

    constructor(name, time_of_birth, location) {
        this.name = name;
        this.coordinates = this.splitLocationString(location);
        if(typeof time_of_birth == 'number'){
            this.time_point = new TimePoint(time_of_birth)
        }

        //Get sidereal time and obliquity
        this.time_point.setSiderealTime(this.coordinates.longitude.decimal_degrees)
        this.obliquity = this.#getObliquity();

        // Get house cusps using Placidus House System
        this.house_cusps = getHouseCusps(
            this.time_point.sidereal_time.decimal_degrees,
            this.coordinates.latitude.decimal_degrees,
            this.obliquity
        );

        // Get planet positions using Ephemeris

    }

    #getObliquity(){ //https://en.wikipedia.org/wiki/Ecliptic#Obliquity_of_the_ecliptic
        let T = this.time_point.julian_century;
        let obliquity;
        obliquity = 84381.406;  // 23°26′21.406″ * 3600 = decimal seconds
        obliquity -= 0.0130102136111111 * T;    // 46.836769″ / 3600 = decimal seconds
        obliquity -= 0.0000000508611111 * Math.pow(T,2);
        obliquity += 0.0000005565 * Math.pow(T,3);
        obliquity -= 0.000000000160 * Math.pow(T,4);
        obliquity -= 0.000000000012056 * Math.pow(T,5);

        return obliquity / 3600;    //Convert from seconds to decimal
    }

    toArray(){
        return {
            'name': this.name,
            'time_of_birth': this.time_point.toArray(),
            'location': this.coordinates,
            'obliquity': this.obliquity,
            'house_cusps': this.house_cusps,
        };
    }

    splitLocationString(location_string){
        let location_string_array = location_string.toString().split(', ');
        return {
            'longitude':new GeoCoordinate(parseFloat(location_string_array[0]),'longitude'),
            'latitude':new GeoCoordinate(parseFloat(location_string_array[1]),'latitude'),
        };
    }
    // getPlanetPositions();
}

// function utcToSiderealDegrees(utc_timestamp, longitude, print = false){
//     let julian_base_day = 2451545;
//
//     //http://www.jgiesen.de/astro/astroJS/siderealClock/
//     let julian_date = utc_timestamp / 86400 + 2440587.5;    //Timestamp in seconds is converted to days and base Julian Day added
//     let julian_year = julian_date / 365.25;
//     console.log(julian_year);
//
//     let theta0;
//     let local_sidereal_time;
//
//     //http://www.agopax.it/Libri_astronomia/pdf/Astronomical%20Algorithms.pdf
//     //Jean Meeus "Astronomical Algorithms" formula 12.3
//     let T = (julian_date - julian_base_day) / 36525;
//     theta0 = 280.46061837 + 360.98564736629 * (julian_date - julian_base_day) + 0.000387933 * (T*T) - (T*T*T) /38710000;
//     let greenwich_sidereal_time = new AngularDistance(theta0);
//
//     //Test at http://www.jgiesen.de/astro/astroJS/siderealClock/
//     local_sidereal_time = new AngularDistance(theta0 + longitude);
//
//     if(print){
//         console.log(
//             "UTC timestamp: "+utc_timestamp+"\n"+
//             "Julian date: "+julian_date+"\n"+
//             "Greenwich Mean Sidereal Time (degrees): "+greenwich_sidereal_time.decimal_degrees+"\n"+
//             "Greenwich Mean Sidereal Time (hours): "+greenwich_sidereal_time.toTimeString()+"\n"+
//             "LST (degrees): "+local_sidereal_time.decimal_degrees+"\n"+
//             "LST (hours): "+local_sidereal_time.toTimeString()+"\n"
//         );
//     }
//
//     return local_sidereal_time.decimal_degrees;
// }

/*
* Takes datetime and adjust it to datetime at the given timezone.
* Returns a timestamp of the moment at UTC 00:00
* Timezone has to use UTC system and be in minutes (e.g. UTC+02:00 -> 120, UTC−09:30 -> −570),
* call with timezone 0 to just remove the browser set timezone.
*/
function dateTimeToUnix(datetime, timezone){

    let unix_timestamp = datetime.getTime() / 1000;  //Get timestamp from date

    //Timezone offsets are given in minutes and a timestamp is in seconds
    unix_timestamp -= datetime.getTimezoneOffset() * 60; //Remove timezone offset made by browser
    unix_timestamp += parseInt(timezone) * 60; //Add timezone offset

    return unix_timestamp;
}

