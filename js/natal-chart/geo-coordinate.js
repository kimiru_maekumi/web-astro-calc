/* Takes geographical coordinate that Google Maps uses and converts it to an angle */

const CARDINALITIES = {
    PosLongitude: 'E',
    NegLongitude: 'W',
    PosLatitude: 'N',
    NegLatitude: 'S',
};

class GeoCoordinate {

    constructor(angle, coordinate_type){
        //Constructing from decimal and type
        if(coordinate_type !== undefined) {
            this.coordinateFromDecimal(angle, coordinate_type);
            return;
        }

        //Constructing from string
        let angle_string = angle.toString();

        //Detect type
        let cardinal_direction = angle_string[angle_string.length - 1];
        if(cardinal_direction === 'E' || cardinal_direction === 'W') this.type = "longitude";
        else this.type = "latitude";

        //Detect sign (+/-)
        let multiplier = 1;
        if(cardinal_direction === 'S' || cardinal_direction === 'W') multiplier = -1;

        //Parse numbers
        let angle_string_array;
        angle_string_array = angle_string.split('°');
        this.degrees = parseInt(angle_string_array[0]);
        angle_string_array = angle_string_array[1];
        this.minutes = parseInt(angle_string_array[1].split('′')[0]);
        this.seconds = 0;
        this.decimal_degrees = this.angleToDecimal() * multiplier;
    }

    coordinateFromDecimal(angle_decimal, coordinate_type){
        this.decimal_degrees = angle_decimal;
        this.type = coordinate_type;
        this.degrees = Math.floor(this.decimal_degrees);
        this.minutes = Math.floor((this.decimal_degrees - this.degrees) * 60);
        this.seconds = Math.floor((this.decimal_degrees - this.degrees - this.minutes / 60) * 3600);
    }

    angleToDecimal(){
        return this.degrees + (this.minutes / 60) + (this.seconds / 3600);
    }


    toString(){
        let string = this.degrees+'°'+this.minutes+"'";
        if(this.type === "longitude"){
            if(this.degrees > 0) return string+CARDINALITIES.PosLongitude;
            else return string+CARDINALITIES.NegLongitude;
        }
        if(this.degrees > 0) return string+CARDINALITIES.PosLatitude;
        else return string+CARDINALITIES.NegLatitude;
    }
    toArray(){
        return {
            'geo-coordinate': {
                'type' : this.type,
                'decimal': this.decimal_degrees,
                'degrees': this.degrees,
                'minutes': this.minutes,
                'seconds': this.seconds,
            }
        }
    }
}