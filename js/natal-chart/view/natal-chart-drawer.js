class NatalChartDrawer{

    chart_size;

    zodiac_outer_circle_radius;
    zodiac_inner_circle_radius;
    zodiac_ring_ratio = 0.25;

    houses_inner_circle_radius;
    houses_ring_ratio = 0.25;
    houses_label_distance = 10;

    axis_node_radius = 38;
    axis_node_gap = 16;
    axis_line_stroke_width = 6;
    axis_node_stroke_width = 3;

    chart_center;

    radians = Math.PI/180;

    zodiac_signs;
    houses;

    constructor(size, zodiac_signs, houses) {

        this.chart_size = size;
        this.calculateRadii();

        this.chart_center = {
            x: this.chart_size / 2,
            y: this.chart_size / 2
        }

        this.zodiac_signs = zodiac_signs;
        this.houses = houses;
        this.redraw();
    }

    calculateRadii(){
        /*

                       houses ring  zodiac ring         gap                axis node
        ⋅              ) 3rd House )     ♎      )-------------------(          Dsc           )
         <--center-->
         <--inner circle radius-->
         <---------outer circle radius---------> <--axis node gap--> <--axis node diameter-->
         <----------------------------------chart radius------------------------------------>

        */
        this.zodiac_outer_circle_radius = (this.chart_size / 2) - this.axis_node_gap - (this.axis_node_radius * 2);
        this.zodiac_inner_circle_radius = this.zodiac_outer_circle_radius - (this.zodiac_outer_circle_radius * this.zodiac_ring_ratio);
        this.houses_inner_circle_radius = this.zodiac_outer_circle_radius - (this.zodiac_outer_circle_radius * (this.zodiac_ring_ratio + this.houses_ring_ratio));

    }

    redraw(){
        this.#scaleChart();
        this.#redrawZodiacRing();
        this.#redrawHousesRing();
        this.#redrawAxis();
    }

    #scaleChart(){
        let chart_element = $("#chart_svg");
        chart_element.css("width",this.chart_size);
        chart_element.css("height",this.chart_size);
    }

    #redrawZodiacRing(){
        //Zodiac circles

        let circle_element = $("#zodiac_outer_circle");

        circle_element.attr("cx",this.chart_center.x);
        circle_element.attr("cy",this.chart_center.y);
        circle_element.attr("r",this.zodiac_outer_circle_radius);

        circle_element = $("#zodiac_inner_circle");

        circle_element.attr("cx",this.chart_center.x);
        circle_element.attr("cy",this.chart_center.y);
        circle_element.attr("r",this.zodiac_inner_circle_radius);

        //Zodiac spokes

        let spoke_line_coords;
        let angle_rad;  //Shortening variable

        let spoke_element;

        for (let i = 15; i <= 345; i+=30) {

            angle_rad = this.radians * i;   //For cos and sin functions

            //Spoke line position points using cos and sin multiplied by radius
            spoke_line_coords = {
                x1: Math.round(Math.cos(angle_rad) * this.zodiac_outer_circle_radius) + this.chart_center.x,
                y1: Math.round(Math.sin(angle_rad) * this.zodiac_outer_circle_radius) + this.chart_center.y,
                x2: Math.round(Math.cos(angle_rad) * this.zodiac_inner_circle_radius) + this.chart_center.x,
                y2: Math.round(Math.sin(angle_rad) * this.zodiac_inner_circle_radius) + this.chart_center.y,
            }

            spoke_element = $("#zodiac_spoke_"+i);
            //Changing the attributes of current element
            spoke_element.attr("x1",spoke_line_coords.x1);
            spoke_element.attr("y1",spoke_line_coords.y1);
            spoke_element.attr("x2",spoke_line_coords.x2);
            spoke_element.attr("y2",spoke_line_coords.y2);
        }

        //Zodiac symbols

        let text_distance = this.zodiac_inner_circle_radius + (this.zodiac_outer_circle_radius - this.zodiac_inner_circle_radius) / 2;
        let text_position, duodecimal;

        let square_radius = (this.zodiac_outer_circle_radius - this.zodiac_inner_circle_radius) / 2;

        let parent = document.getElementById("zodiac_labels");  //Element containing all the symbol elements
        let sample_element = document.getElementById("zodiac_label_sample");    //Element already in HTML to copy over for each symbol
        let clone_element, rect_element, text_element;

        for (let i = 0; i <= 330; i+=30) {

            duodecimal = NatalChartDrawer.#degreesToDuodecimal(i,3); //Convert degrees to positions on a clock

            angle_rad = this.radians * i;   //For cos and sin functions

            //Get new clone from sample element
            clone_element = sample_element.cloneNode(true);
            clone_element.setAttributeNS(null,"id","zodiac_text_"+i);

            //Spoke line position points using cos and sin multiplied by radius
            text_position = {
                x: Math.round(Math.cos(angle_rad) * text_distance) + this.chart_center.x,
                y: Math.round(Math.sin(-angle_rad) * text_distance) + this.chart_center.y,  //In trigonometry chart 0y is at bottom
            }

            //Set attributes of rect element
            rect_element = clone_element.querySelector('rect');
            rect_element.setAttributeNS(null,"x",(text_position.x - square_radius).toString());
            rect_element.setAttributeNS(null,"y",(text_position.y - square_radius).toString());
            rect_element.setAttributeNS(null,"width",(square_radius * 2).toString());
            rect_element.setAttributeNS(null,"height",(square_radius * 2).toString());

            //Find zodiac sign associated clock position
            for (let j = 0; j < this.zodiac_signs.length; j++){
                if(parseInt(this.zodiac_signs[j].order) === duodecimal) {

                    //Set attributes of text element
                    text_element = clone_element.querySelector('text');
                    text_element.innerHTML = zodiac_signs[j].symbol;
                    text_element.setAttributeNS(null,"x",text_position.x.toString());
                    text_element.setAttributeNS(null,"y",text_position.y.toString());

                    parent.appendChild(clone_element);
                    break;  //Stop searching for zodiac sign and move on to next position
                }
            }
        }

        sample_element.remove();
    }

    #redrawHousesRing(){
        //Houses circles

        let circle_element = $("#houses_inner_circle");

        circle_element.attr("cx",this.chart_center.x);
        circle_element.attr("cy",this.chart_center.y);
        circle_element.attr("r",this.houses_inner_circle_radius);

        //Houses spokes

        let spoke_line_coords;
        let spoke_line_pair_coords = [];

        //Spoke one end at 0°
        spoke_line_coords = {
            x1: Math.round(Math.cos(0) * this.zodiac_inner_circle_radius) + this.chart_center.x,
            y1: Math.round(Math.sin(0) * this.zodiac_inner_circle_radius) + this.chart_center.y,
            x2: Math.round(Math.cos(0) * this.houses_inner_circle_radius) + this.chart_center.x,
            y2: Math.round(Math.sin(0) * this.houses_inner_circle_radius) + this.chart_center.y,
        }
        spoke_line_pair_coords.push(spoke_line_coords);
        //Spoke other end at 180°
        spoke_line_coords = {
            x1: Math.round(Math.cos(Math.PI) * this.zodiac_inner_circle_radius) + this.chart_center.x,
            y1: Math.round(Math.sin(Math.PI) * this.zodiac_inner_circle_radius) + this.chart_center.y,
            x2: Math.round(Math.cos(Math.PI) * this.houses_inner_circle_radius) + this.chart_center.x,
            y2: Math.round(Math.sin(Math.PI) * this.houses_inner_circle_radius) + this.chart_center.y,
        }
        spoke_line_pair_coords.push(spoke_line_coords);

        let text_positions;

        //Apply the same line point positions to all spokes and then rotate them
        for (let i = 0; i < 180; i+=30) {
            NatalChartDrawer.#setLineAttributes(document.querySelector("#houses_pair_"+i+" > line.houses-spoke-a"),spoke_line_pair_coords[0]);
            NatalChartDrawer.#setLineAttributes(document.querySelector("#houses_pair_"+i+" > line.houses-spoke-b"),spoke_line_pair_coords[1]);

            //Labels

            text_positions = {
                x: spoke_line_pair_coords[0].x2 + this.houses_label_distance,
                y: spoke_line_pair_coords[0].y2 - this.houses_label_distance,
            }
            NatalChartDrawer.#setPositionAttributes(document.querySelector("#houses_pair_"+i+" > text.houses-text-a"),text_positions);
            text_positions = {
                x: spoke_line_pair_coords[1].x2 - this.houses_label_distance,
                y: spoke_line_pair_coords[1].y2 + this.houses_label_distance,
            }
            NatalChartDrawer.#setPositionAttributes(document.querySelector("#houses_pair_"+i+" > text.houses-text-b"),text_positions);

            document.querySelector("#houses_pair_"+i).style.transform = "rotate("+(-i)+"deg)";
            document.querySelector("#houses_pair_"+i+" > text.houses-text-a").style.transform = "rotate("+(i)+"deg)";
            document.querySelector("#houses_pair_"+i+" > text.houses-text-b").style.transform = "rotate("+(i)+"deg)";
            document.querySelector("#houses_pair_"+i+" > text.houses-text-a > tspan").innerHTML = NatalChartDrawer.#degreesToDuodecimal(-i,7);
            document.querySelector("#houses_pair_"+i+" > text.houses-text-b > tspan").innerHTML = NatalChartDrawer.#degreesToDuodecimal(180 - i,7);
        }

    }


    //Returns which number on a clock is shown given offset in degrees and where the 0° point is at
    static #degreesToDuodecimal(degrees_offset, zero_at){
        var sections = 12;
        //Quotient from division of given angle by degrees for each section (degrees / 30)
        var duodecimal = NatalChartDrawer.#wrapDegrees(degrees_offset);
        //Reorder clockwise like on a clock
        duodecimal = 11 - duodecimal;
        //Offset number by the starting point
        duodecimal -= (sections - zero_at - 1);
        if(duodecimal <= 0) duodecimal = sections + duodecimal; //If goes in the negatives wrap the number range around
        return duodecimal;
    }


    //Maps degrees in plane angle to duodecimal sections (0° -> 0; 90° -> 3; 330° -> 11)
    static #wrapDegrees(degrees){
        var sections = 12;
        var section_degrees = 360 / sections;   //30°
        let division = degrees / section_degrees;   //Floating point value to be rounded
        //If remainder is more than half of a section rounds up
        if(degrees % section_degrees > section_degrees / 2) {
            //If rounding up results in section index out of bounds wraps around
            if(Math.ceil(division) === sections) return 0;
            return Math.ceil(division);
        }
        //If remainder is less than half of a section rounds down
        return Math.floor(division);
    }

    #redrawAxis(){
        this.#redrawAxisLines();
        this.#alignAxisNodeSymbols();
        this.#alignAxisNodeTexts();
    }

    #redrawAxisLines(){
        let axis_line_distance = this.zodiac_outer_circle_radius + this.axis_node_gap;

        let line_points = [];
        //Equator line start and end positions
        line_points.push({
            x1: this.chart_center.x + axis_line_distance,
            y1: this.chart_center.y,
            x2: this.chart_center.x - axis_line_distance,
            y2: this.chart_center.y,
        });
        //Meridian line start and end positions
        line_points.push({
            x1: this.chart_center.x,
            y1: this.chart_center.y - axis_line_distance,
            x2: this.chart_center.x,
            y2: this.chart_center.y + axis_line_distance,
        });

        //Line elements
        let elements = [
            document.querySelector("#equator_lines_layer > line:first-child"),
            document.querySelector("#meridian_lines_layer > line:first-child")
        ];

        //Change line appearance
        for (let i = 0; i < 2; i++) {
            elements[i].setAttributeNS(null, "stroke-width",this.axis_line_stroke_width.toString());
            NatalChartDrawer.#setLineAttributes(elements[i],line_points[i]);
        }

        //Change line gradient attributes
        //Equator line gradient = horizontal gradient
        line_points[0] = {
            x1: this.chart_center.x,
            y1: this.chart_center.y - this.axis_line_stroke_width,
            x2: this.chart_center.x,
            y2: this.chart_center.y + this.axis_line_stroke_width,
        };
        //Meridian line gradient = vertical gradient
        line_points[1] = {
            x1: this.chart_center.x - this.axis_line_stroke_width,
            y1: this.chart_center.y,
            x2: this.chart_center.x + this.axis_line_stroke_width,
            y2: this.chart_center.y,
        };

        //Gradient elements
        elements = [
            document.querySelector("#horizontal_glass_gradient"),
            document.querySelector("#vertical_glass_gradient")
        ];

        for (let i = 0; i < 2; i++) {
            NatalChartDrawer.#setLineAttributes(elements[i],line_points[i]);
        }

        //Circles
        let axis_nodes = document.getElementsByClassName("axis-node");
        let circles = document.querySelectorAll(".axis-node > .axis-node-circle");

        let node_positions = [];

        let axis_node_distance = this.zodiac_outer_circle_radius + this.axis_node_gap;
        //Ascendant node position
        node_positions.push({
            x: this.chart_center.x - axis_node_distance - this.axis_node_radius * 2,
            y: this.chart_center.y - this.axis_node_radius
        });
        //Descendant node position
        node_positions.push({
            x: this.chart_center.x + axis_node_distance,
            y: this.chart_center.y - this.axis_node_radius
        });
        //Medium Coeli node position
        node_positions.push({
            x: this.chart_center.x - this.axis_node_radius,
            y: this.chart_center.y - axis_node_distance - this.axis_node_radius * 2
        });
        //Imum Coeli node position
        node_positions.push({
            x: this.chart_center.x - this.axis_node_radius,
            y: this.chart_center.y + axis_node_distance
        });

        let radius = this.axis_node_radius - this.axis_node_stroke_width / 2;

        for (let i = 0; i < 4; i++) {
            NatalChartDrawer.#setPositionAttributes(axis_nodes[i],node_positions[i]);
            NatalChartDrawer.#setSizeAttributes(axis_nodes[i],this.axis_node_radius * 2, this.axis_node_radius * 2);

            circles[i].setAttributeNS(null, "r", radius.toString());
            circles[i].setAttributeNS(null, "stroke-width",this.axis_node_stroke_width.toString());
            circles[i].setAttributeNS(null,"cx",this.axis_node_radius.toString());
            circles[i].setAttributeNS(null,"cy",this.axis_node_radius.toString());
        }
    }

    #alignAxisNodeSymbols(){
        let rect_offset = {
            x: this.axis_node_radius - 24,
            y: this.axis_node_radius - 24
        };
        let axis_node_symbols = document.getElementsByClassName("axis-symbol");

        for (let i = 0; i < 4; i++) {
            NatalChartDrawer.#setPositionAttributes(axis_node_symbols[i],rect_offset);
        }
    }
    #alignAxisNodeTexts(){
        //Texts

        let text_elements = document.getElementsByClassName("axis-node-text");

        let text_position = {
            x: 24,
            y: 24 + text_elements[0].getBoundingClientRect().height / 4
        }
        for (let i = 0; i < text_elements.length; i++) {
            NatalChartDrawer.#setPositionAttributes(text_elements[i], text_position);
        }
    }

    static #setLineAttributes(element, line_points){
        element.setAttributeNS(null,"x1",line_points.x1.toString());
        element.setAttributeNS(null,"y1",line_points.y1.toString());
        element.setAttributeNS(null,"x2",line_points.x2.toString());
        element.setAttributeNS(null,"y2",line_points.y2.toString());
    }

    static #setPositionAttributes(element, position_point){
        element.setAttributeNS(null,"x",position_point.x.toString());
        element.setAttributeNS(null,"y",position_point.y.toString());
    }

    static #setSizeAttributes(element, width, height){
        element.setAttributeNS(null,"width",width.toString());
        element.setAttributeNS(null,"height",height.toString());
    }



}

$( window ).on( "load", function() {
    let chart_drawer = new NatalChartDrawer(900, zodiac_signs, houses);

    //Show elements after they have been transformed to avoid flickering
    let chart_svg_element = $("#chart_svg");
    chart_svg_element.addClass("fade-in");
    chart_svg_element.removeClass("transparent");
});