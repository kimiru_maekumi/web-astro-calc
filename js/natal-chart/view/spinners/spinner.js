class Spinner{

    //Points
    centre_of_rotation; //The middle of the element that needs spinning
    relative_position;  //Mouse position relative to centre of rotation

    is_spinning = false;

    //Angles
    start_angle = 0;    //Angle upon when user clicks the element
    angle_increase = 0; //Amount by which to increase starting angle
    current_angle = 0;  //Angle after the rotation has been completed

    radians = 180 / Math.PI;

    startSpin(bounding_rect, event){
        //The position of the axis around which to spin the element
        this.centre_of_rotation = {
            x: bounding_rect.left + (bounding_rect.width / 2),
            y: bounding_rect.top + (bounding_rect.height / 2)
        };

        //Get mouse position as if it was placed on the trigonometry chart
        this.calculateRelativePosition(event);

        //The script doesn't arrangement of svg elements so a click calculates the angle
        this.start_angle = this.radians * Math.atan2(this.relative_position.y, this.relative_position.x);
    }

    //From global mouse coordinates to mouse coordinates local relative to center of rotation
    calculateRelativePosition(event){
        this.relative_position = {
            x: event.clientX - this.centre_of_rotation.x,
            y: event.clientY - this.centre_of_rotation.y
        }
    }

    stopSpin(){
        this.current_angle += this.angle_increase;  //Apply new angle
        this.is_spinning = false;  //Stop user from affecting the rotation
        this.angle_increase = 0;    //Reset angle increase
    }

    //Get the amount user has moved the element from start to current position in degrees
    continueSpin(event){
        this.calculateRelativePosition(event);
        this.angle_increase = this.radians * Math.atan2(this.relative_position.y, this.relative_position.x) - this.start_angle;
    }
}