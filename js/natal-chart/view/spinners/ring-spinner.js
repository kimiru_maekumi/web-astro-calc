class RingSpinner extends Spinner {

    spinning_element;
    symbol_angle = 0;

    //OVERRIDES
    constructor(element) {
        super();
        this.spinning_element = element;
    }
    startSpin(event){
        super.startSpin(this.spinning_element.get(0).getBoundingClientRect(), event);

        //Check if clicked inside the ring
        let outer_radius = document.querySelector("#zodiac_outer_circle").getAttribute("r");
        let inner_radius = document.querySelector("#zodiac_inner_circle").getAttribute("r");

        //Calculate the distance from center of circles to the position of mouse
        let hypotenuse = Math.sqrt(this.relative_position.x*this.relative_position.x + this.relative_position.y*this.relative_position.y);
        //Check if clicked outside inner circle and inside the outer circle
        if(hypotenuse > inner_radius && hypotenuse < outer_radius) this.is_spinning = true;
    }
    continueSpin(event){
        super.continueSpin(event);
        //Actually change the element's visual
        this.transformRotation(this.angle_increase);
    }
    stopSpin(){
        //Update new angle of symbol (has to be done before the angle increase is reset)
        this.symbol_angle -= this.angle_increase;
        super.stopSpin();
    }

    //UNIQUE FUNCTIONS
    transformRotation(degrees){
        this.spinning_element.css({
            '-webkit-transform' : 'rotate('+ (this.current_angle + degrees) +'deg)',
            '-moz-transform' : 'rotate('+ (this.current_angle +  degrees) +'deg)',
            '-ms-transform' : 'rotate('+ (this.current_angle +  degrees) +'deg)',
            'transform' : 'rotate('+ (this.current_angle +  degrees) +'deg)'});
        // let text_elements = document.getElementsByClassName("zodiac-symbol");
        $(".zodiac-symbol").css({
            '-webkit-transform' : 'rotate('+ (this.symbol_angle - degrees) +'deg)',
            '-moz-transform' : 'rotate('+ (this.symbol_angle - degrees) +'deg)',
            '-ms-transform' : 'rotate('+ (this.symbol_angle - degrees) +'deg)',
            'transform' : 'rotate('+ (this.symbol_angle - degrees) +'deg)'});
    }
}

let ring_spinner_element = $("#zodiac_ring");

const ring_spinner = new RingSpinner(ring_spinner_element);

//Mouse events
ring_spinner_element.mousedown(function(event) {
    ring_spinner.startSpin(event);
});
$(document).mousemove(function(event){
    if(ring_spinner.is_spinning) ring_spinner.continueSpin(event);
});
$(document).mouseup(function(event){
    if(ring_spinner.is_spinning) ring_spinner.stopSpin(event);
});

$(".axis > line").mousedown(function(event) {
    ring_spinner.startSpin(event);
});
$("#chart_svg").mousedown(function(event) {
    ring_spinner.startSpin(event);
});