axis_names = {
    AXIS_EQUATOR: 0,
    AXIS_MERIDIAN: 1,
};

house_angles = [
    {
        element: "#houses_pair_0",
        angle : 0
    },
    {
        element: "#houses_pair_30",
        angle : 30
    },
    {
        element: "#houses_pair_60",
        angle : 60
    },
    {
        element: "#houses_pair_90",
        angle : 90
    },
    {
        element: "#houses_pair_120",
        angle : 120
    },
    {
        element: "#houses_pair_150",
        angle : 150
    },
];

class AxisSpinner extends Spinner {

    axis_name;

    selected_axis;
    symbols;
    symbol_angle = 0;

    //OVERRIDES
    constructor(axis_name) {
        super();
        this.axis_name = axis_name;
    }
    startSpin(event){
        super.startSpin(this.selected_axis[0].getBoundingClientRect(), event);
        this.is_spinning = true;
    }
    continueSpin(event){
        super.continueSpin(event);
        //Actually change the element's visual
        this.transformRotation(this.angle_increase);
    }
    stopSpin(){
        //Update new angle of symbol (has to be done before the angle increase is reset)
        this.symbol_angle -= this.angle_increase;
        super.stopSpin();
    }

    //UNIQUE FUNCTIONS
    transformRotation(degrees){
        //Rotate axis
        this.rotateElement(this.selected_axis,this.current_angle + degrees);
        //Rotate symbols in the opposite direction
        this.rotateElement(this.symbols,this.symbol_angle - degrees);
    }

    rotateElement(element, degrees){
        element.css({
            '-webkit-transform' : 'rotate('+ degrees +'deg)',
            '-moz-transform' : 'rotate('+ degrees +'deg)',
            '-ms-transform' : 'rotate('+ degrees +'deg)',
            'transform' : 'rotate('+ degrees +'deg)'});
    }
}

const equator_spinner = new AxisSpinner(axis_names.AXIS_EQUATOR);
const meridian_spinner = new AxisSpinner(axis_names.AXIS_MERIDIAN);

meridian_spinner.start_angle = 90;

//Mouse events
$(".axis-node-circle").mousedown(function(event) {

    if(this.parentElement.parentElement.id === "equator_layer") {
        //Find all symbols (not just in children) that are supposed to rotate in the opposite direction of axis rotation
        equator_spinner.symbols = $("#"+this.parentElement.parentElement.id+" .rotate-inverse");
        //Element to spin
        equator_spinner.selected_axis = $("#"+this.parentElement.parentElement.id);
        equator_spinner.startSpin(event);
    }
    else {
        //Find all symbols (not just in children) that are supposed to rotate in the opposite direction of axis rotation
        meridian_spinner.symbols = $("#"+this.parentElement.parentElement.id+" .rotate-inverse");
        //Element to spin
        meridian_spinner.selected_axis = $("#"+this.parentElement.parentElement.id);
        meridian_spinner.startSpin(event);
    }

});
$(document).mousemove(function(event){

    //Only rotate if user is currently dragging
    if(equator_spinner.is_spinning) equator_spinner.continueSpin(event);
    else if(meridian_spinner.is_spinning) meridian_spinner.continueSpin(event);
});
$(document).mouseup(function(event){
    if(equator_spinner.is_spinning) equator_spinner.stopSpin(event);
    else if(meridian_spinner.is_spinning) meridian_spinner.stopSpin(event);
});