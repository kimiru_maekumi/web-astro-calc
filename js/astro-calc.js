//OOP Classes

class HouseOrSign {
  constructor(xml_node) {  
	var id = xml_node.getAttribute("id");
    this.id = id;
    this.name = xml_node.getAttribute("name");
    this.symbol = xml_node.getAttribute("symbol");
    this.order = xml_node.getAttribute("order");
	this.element = id % 4;
	this.quality = id % 3;
	this.polarity = id % 2;
  }
}

class Planet {
  constructor(xml_node) {	 
    this.id = xml_node.getAttribute("id");
    this.number = xml_node.getAttribute("number");
    this.name = xml_node.getAttribute("name");
    this.symbol = xml_node.getAttribute("symbol");
  }
}

/*
Element enumerator
FIRE 1, 5, 9
EARTH 2, 5, 10
AIR 3, 7, 11
WATER 4, 8, 12
ELEMENT = i % 4
*/
const Elements = {
   WATER: 0,
   EARTH: 1,
   FIRE: 2,
   AIR: 3,
};

/*
Qualities enumerator
CARDINAL 1, 4, 7, 10
FIXED 2, 5, 8, 11
MUTABLE 3, 6, 9, 12
QUALITY = i % 3
*/
const Qualities = {
   MUTABLE: 0,
   CARDINAL: 1,
   FIXED: 2,
};

/*
Polarities enumerator
ACTIVE 1, 3, 5, 7, 9, 11
PASSIVE 2, 4, 6, 8, 10, 12
POLARITY = i % 2
*/
const Polarities = {
   PASSIVE: 0,
   ACTIVE: 1,
};

let zodiac_signs = [], houses = [], planets = [];

//Call function when loading document
$( window ).on( "load", function() {
    prepareCalc();
});

function prepareCalc(){
	//Arrays for calculator to avoid global variables
	
	const parser = new DOMParser();
	const dom = parser.parseFromString(document.getElementById('item_string_data').innerHTML, "application/xml");
	
	let xml_nodes;
	xml_nodes = dom.getElementById("signs").children;
	for (let i = 0; i< xml_nodes.length; i++) zodiac_signs.push(new HouseOrSign(xml_nodes[i]));
	xml_nodes = dom.getElementById("houses").children;
	for (let i = 0; i< xml_nodes.length; i++) houses.push(new HouseOrSign(xml_nodes[i]));
	xml_nodes = dom.getElementById("planets").children;
	for (let i = 0; i< xml_nodes.length; i++) planets.push(new Planet(xml_nodes[i]));

}

/*
* LOAD TIME RELATED HTML
*/

//Call function when loading document
$( window ).on( "load", function() {
    fillTimezoneOptions();
});

function fillTimezoneOptions(){
    //Arrays for calculator to avoid global variables

    const parser = new DOMParser();
    const dom = parser.parseFromString(document.getElementById('timezone_data').innerHTML, "application/xml");

    let timezone_select = document.getElementById("timezone_select");

    let xml_nodes;
    xml_nodes = dom.getElementById("timezones").children;
    for (let i = 0; i< xml_nodes.length; i++) {
        let new_option = document.createElement("option");

        new_option.innerHTML = xml_nodes[i].getAttribute("name");
        new_option.value = xml_nodes[i].getAttribute("value");

        timezone_select.appendChild(new_option);
    }

}

let time_picker_element = $("#time");
let time_picker = flatpickr(time_picker_element, {
    enableTime: true,
    enableSeconds: true,
    dateFormat: "d/m/Y H:i:S",
    time_24hr: true,
}); // flatpickr
let timezone_element = $("#timezone_select");
let latitude_element = $("#latitude");
let longitude_element = $("#longitude");

const PI = Math.PI;

/*
* PLACIDUS HOUSE CUSPS CALCULATION
*/

//Calculate event
$("#calculate_btn").click(function() {

    let time_input_by_user = time_picker.parseDate(time_picker_element.val(), "d/m/Y H:i:S");
    let timezone_input_by_user = timezone_element.val();
    // let utc_timestamp = utcAtTimezone(new Date(time_input_by_user), timezone_input_by_user);
    //https://www.epochconverter.com/
    // let utc_timestamp = new Date().getTime() / 1000;  //Get timestamp from date
    // let utc_timestamp = new Date(time_input_by_user).getTime() / 1000;  //Get timestamp from date
    let utc_timestamp = 654908400;  //Get timestamp from date

    // let longitude = parseFloat(longitude_element.val());
    // let longitude = 13.41;
    let longitude = 13.37772;
    let latitude = 52.51628;

    let ecliptic_obliquity = 23.44210092; //Decimal degrees

    setNatalChart(utc_timestamp, longitude, latitude, ecliptic_obliquity);
});

function setNatalChart(utc_timestamp, longitude, latitude, ecliptic_obliquity){
    //Find how far from the vernal equinox the Medium Coeli is on the celestial equator (3D)
    // let ramc_degrees = utcToSiderealDegrees(utc_timestamp,longitude, true);
    let ramc_degrees = utcToSiderealDegrees(utc_timestamp,longitude);

    //Using RAMC and latitude find where all the necessary cusps are in two dimensions
    let placidus_houses = new PlacidusHouses(ramc_degrees, latitude, ecliptic_obliquity);
}